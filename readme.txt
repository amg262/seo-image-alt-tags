=== SEO Image Toolbox ===



Contributors: amg26



Tags: seo images, seo, optimize images, image seo, image tags, alt tags, media, search engine optimization, alt, image seo,
seo alt tag, html validation, optimize images, seo toolbox, image toolbox, seo images, seo tags, tags, image tags



Donate link: andrewgunn.org/donate



Requires at least: 3.0



Tested up to: 4.7.2



Stable tag: 3.2.6



License: GPLv2 or later



License URI: http://www.gnu.org/licenses/gpl-2.0.html



THIS CAN SAVE YOU HOURS, DAYS, 0R WEEKS.


Improve your site's SEO and save hours of time spent manually entering ALT tags with the best
solution for completely Search Engine Optimizing images and dynamically fixing HTML validation errors.


SEO Image Tags puts an end to ever have to worry about getting HTML validation errors for images
and improves your SEO score by completely optimizing image data. Alt tags are dynamically generated
 and saved to the database automatically any time an image is uploaded, no configuration or headache.





== Description ==

THIS CAN SAVE YOU HOURS, DAYS, 0R WEEKS.


Have you spent hours manually filling ALT tags for your images to boost SEO? Now it takes less than 5 seconds.
SEO Image Toolbox is capable of handle 7,000 images missing ALT tags in 15 seconds.


SEO Image Tags puts an end to ever have to worry about getting HTML validation errors for images
and improves your SEO score by completely optimizing image data. Alt tags are dynamically generated
 and saved to the database automatically any time an image is uploaded, no configuration or headache.


You can run the database updater to create, update, or delete image tag data for all images in the media
library. All with a lightweight and efficient clientside script to EVERY image is properly tagged.







== Installation ==



Either upload a .zip file, or install through the WordPress.org repository.



